# CocoaPods Podspecs for Glance iOS and MacOS SDKs

This is Glance's release repository.  You may prefer to use the CocoaPods.org Podspecs at:

[Glance_iOS](https://github.com/CocoaPods/Specs/tree/master/Specs/3/3/4/Glance_iOS)

[Glance_iOSVideo](https://github.com/CocoaPods/Specs/tree/master/Specs/1/f/f/Glance_iOSVideo )
