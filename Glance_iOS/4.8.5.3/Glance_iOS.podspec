Pod::Spec.new do |s|
  s.name             = 'Glance_iOS'
  s.version          = '4.8.5.3'
  s.summary          = 'Glance iOS SDK'
  s.description      = <<-DESC
Glance iOS SDK
                       DESC
  s.homepage         = 'https://www.glance.net'
  s.author           = { 'Glance Networks' => 'glance@glance.net' }
  s.ios.deployment_target = '9.0'
  s.license      = {
    :type => "Commercial",
    :text => "Copyright © 2017-2020 Glance Networks, Inc. All rights reserved. Use of this software is subject to the terms and conditions of the Glance Terms of Service located at https://ww2.glance.net/terms"
  }
  s.source       = { :http    => "https://gitlab.com/glance-networks/CocoaPodsSpecs/-/raw/master/Glance_iOS/4.8.5.3/Glance_iOS.framework_4.8.5.3.zip" }
  s.vendored_frameworks   = "Glance_iOS.framework"
  s.requires_arc          = true
  s.xcconfig              = { 'OTHER_LDFLAGS' => '-ObjC' }
  s.frameworks = 'AVFoundation'
end
