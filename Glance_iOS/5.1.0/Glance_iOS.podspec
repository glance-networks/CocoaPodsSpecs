Pod::Spec.new do |s|
  s.name             = 'Glance_iOS'
  s.version          = '5.1.0'
  s.summary          = 'Glance iOS SDK'
  s.description      = <<-DESC
Glance iOS SDK for screensharing, voice and video display.
Use Glance_iOSVideo if video generation is needed
                       DESC
  s.homepage         = 'https://www.glance.net'
  s.author           = { 'Glance Networks' => 'glance@glance.net' }
  s.ios.deployment_target = '10.0'
  s.license      = {
    :type => "Commercial",
    :text => "Copyright © 2017-2021 Glance Networks, Inc. All rights reserved. Use of this software is subject to the terms and conditions of the Glance Terms of Service located at https://ww2.glance.net/terms"
  }
  s.source       = { :http    => "https://gitlab.com/glance-networks/CocoaPodsSpecs/-/raw/master/Glance_iOS/5.1.0/Glance_iOS.xcframework_5.1.0.zip" }
  s.vendored_frameworks   = "Glance_iOS.xcframework"
  s.requires_arc          = true
  s.xcconfig              = { 'OTHER_LDFLAGS' => '-ObjC' }
  s.pod_target_xcconfig   = { 'ONLY_ACTIVE_ARCH' => 'YES' }
  s.frameworks = 'AVFoundation'
end

